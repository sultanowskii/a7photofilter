Photo Filter Bot - бот в Telegram, который обрабатывает ваши фотографии.

Сейчас бот неактивен, поддерживать бота я не собираюсь, репозиторий оставляю открытым, по всем интересующим вопросам пишите сюда: t.me/sultanowskii

Разработчики: Султанов Артур и Габидуллин Камиль

Благодарим Мусина Искандера за помощь и консультацию в создании проекта.

Если у вас остались какие-нибудь вопросы, то пишите в Telegram:
Султанов Артур - t.me/sultanowskii
Габидуллин Камиль -  t.me/gabidullin_kamil
